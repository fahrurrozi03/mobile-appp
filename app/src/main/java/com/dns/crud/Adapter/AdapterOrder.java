package com.dns.crud.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dns.crud.DetailOderActivity;
import com.dns.crud.MainActivity;
import com.dns.crud.Model.DataOrder;
import com.dns.crud.R;

import java.util.List;

public class AdapterOrder extends RecyclerView.Adapter<AdapterOrder.HolderData> {

    private List<DataOrder> mList;
    private Context ctx;

    public AdapterOrder (Context ctx, List<DataOrder> mList) {

        this.ctx = ctx;
        this.mList = mList;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.member_list,parent, false);
        HolderData holder = new HolderData(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        DataOrder dm = mList.get(position);
        holder.order_name.setText(dm.getOrder_name());
        holder.order_type.setText(dm.getOrder_type());
        holder.dm = dm;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    class HolderData extends RecyclerView.ViewHolder{

        TextView order_name , order_type, order_id;

        DataOrder dm;
        public HolderData (View v)
        {
            super(v);

            order_id  = (TextView) v.findViewById(R.id.tvId);
            order_name  = (TextView) v.findViewById(R.id.tvNama);
            order_type = (TextView) v.findViewById(R.id.tvType);

            Toast.makeText(ctx, "", Toast.LENGTH_SHORT).show();

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent goInput = new Intent(ctx, DetailOderActivity.class);
                    goInput.putExtra("order_id", dm.getOrder_id());
                    goInput.putExtra("order_name", dm.getOrder_name());
                    goInput.putExtra("order_type", dm.getOrder_type());
                    ctx.startActivity(goInput);
                }
            });
        }
    }
}
