package com.dns.crud;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.zxing.WriterException;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.williammora.snackbar.Snackbar;

import me.ydcool.lib.qrmodule.encoding.QrGenerator;

public class Akun extends AppCompatActivity implements View.OnClickListener {

    private ImageView qrcode,refreshqr,img_fotoprofile;
    private EditText ed_nama, ed_email;
    private Button update_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_akun);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);     //  Fixed Portrait orientation

        qrcode = findViewById(R.id.qrcode);
        refreshqr = findViewById(R.id.refresh_qrcode);

        ed_nama = findViewById(R.id.ed_nama);
        ed_email = findViewById(R.id.ed_email);
        img_fotoprofile = findViewById(R.id.fotoprofile);

        update_btn = findViewById(R.id.updateakun);
        update_btn.setOnClickListener(this);

        ed_nama.setText(dataUser.getRegisteredUser(getBaseContext()));
        ed_email.setText(dataUser.getRegisteredEmail(getBaseContext()));

//        Resources r = getResources();
//        Drawable[] layers = new Drawable[2];
//        layers[0] = r.getDrawable(R.drawable.person_foreground);
//        layers[1] = r.getDrawable(R.drawable.ic_create_black_24dp);
//        LayerDrawable layerDrawable = new LayerDrawable(layers);
        img_fotoprofile.setImageResource(R.drawable.person_foreground);

        showQrcode();

        refreshqr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RotateAnimation rotate = new RotateAnimation(0, 170, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                rotate.setDuration(998);
                rotate.setInterpolator(new LinearInterpolator());
                refreshqr.startAnimation(rotate);
                updateQr(rotate);

            }
        });

    }

    private void updateQr(RotateAnimation rotate) {

        final int random = 1;

        String qrcodeimg = dataUser.getRegisteredQrcode(getBaseContext()) + random;

        Bitmap qrCode = null;
        try {
            qrCode = new QrGenerator.Builder()
                    .content(qrcodeimg)
                    .qrSize(150)
                    .margin(2)
                    .color(Color.BLACK)
                    .bgColor(Color.WHITE)
                    .ecc(ErrorCorrectionLevel.H)
                    .overlaySize(100)
                    .overlayAlpha(255)
                    .overlayXfermode(PorterDuff.Mode.SRC_ATOP)
                    .encode();
        } catch (WriterException e) {
            e.printStackTrace();
        }

        dataUser.setRegisteredQrcode(getBaseContext(), qrcodeimg);

        qrcode.setImageBitmap(qrCode);
        rotate.hasEnded();
    }

    private void showQrcode() {

        final String qrcodefirst = dataUser.getRegisteredQrcode(getBaseContext());

        if(qrcodefirst.isEmpty()){
            String qrcodeimgs = dataUser.getLoggedInUser(getBaseContext());

            Bitmap qrCode = null;
            try {
                qrCode = new QrGenerator.Builder()
                        .content(qrcodeimgs)
                        .qrSize(150)
                        .margin(2)
                        .color(Color.BLACK)
                        .bgColor(Color.WHITE)
                        .ecc(ErrorCorrectionLevel.H)
                        .overlaySize(100)
                        .overlayAlpha(255)
                        .overlayXfermode(PorterDuff.Mode.SRC_ATOP)
                        .encode();
            } catch (WriterException e) {
                e.printStackTrace();
            }
            qrcode.setImageBitmap(qrCode);
        } else {

            String qrcodeimgupdate = dataUser.getRegisteredQrcode(getBaseContext());

            Bitmap qrCodes = null;
            try {
                qrCodes = new QrGenerator.Builder()
                        .content(qrcodeimgupdate)
                        .qrSize(150)
                        .margin(2)
                        .color(Color.BLACK)
                        .bgColor(Color.WHITE)
                        .ecc(ErrorCorrectionLevel.H)
                        .overlaySize(100)
                        .overlayAlpha(255)
                        .overlayXfermode(PorterDuff.Mode.SRC_ATOP)
                        .encode();
            } catch (WriterException e) {
                e.printStackTrace();
            }
            qrcode.setImageBitmap(qrCodes);

        }

    }

    @Override
    public void onClick(View v) {
        if(v == update_btn) {

            final String nama_profile = ed_nama.getText().toString().trim();
            final String email_profile = ed_email.getText().toString().trim();

            dataUser.setRegisteredUser(getBaseContext(), nama_profile);
            dataUser.setRegisteredEmail(getBaseContext(), email_profile);

            Snackbar.with(Akun.this)
                    .text("Profile kamu berhasil di update")
                    .show(Akun.this);
            Intent back = new Intent(Akun.this, dashboard.class);
            startActivity(back);
            finish();
        }

    }
}
