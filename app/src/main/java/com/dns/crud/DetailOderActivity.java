package com.dns.crud;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.dns.crud.Model.DataOrder;

public class DetailOderActivity extends AppCompatActivity {

    private TextView mTitle,mType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_oder);

        mTitle = findViewById(R.id.titleOrder);
        mType  = findViewById(R.id.type_order);

        String order_id = getIntent().getStringExtra("order_id");
        String order_name = getIntent().getStringExtra("order_name");
        String order_type = getIntent().getStringExtra("order_type");

        mTitle.setText(order_name);
        mType.setText(order_type);

    }
}
