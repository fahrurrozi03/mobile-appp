package com.dns.crud.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.dns.crud.Adapter.PhotoAdapter;
import com.dns.crud.Model.PhotoData;
import com.dns.crud.R;
import com.dns.crud.Rest.ApiClient;
import com.dns.crud.Rest.GetService;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.williammora.snackbar.Snackbar;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {

    private TextView txt_ctg;
    private TextView nama, nope;
    private String nohp;
    public TelephonyManager telephonyManager;
    private Button logoutt;
    ProgressBar progressBar;
    private ImageView img_qr;
    PorterDuff porterDuff;
    private Button cekDat;
    private FloatingActionButton fabmenu;
    private String JSON_STRING;
    private PhotoAdapter adapter;
    private RecyclerView recyclerView;
    private CardView sliderUp, cardAtas;
    ProgressDialog progressDoalog;
    public NavigationView navigationView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_home, container, false);

        txt_ctg = v.findViewById(R.id.categories);

        cardAtas = v.findViewById(R.id.card);
        nama = v.findViewById(R.id.name);
        nope = v.findViewById(R.id.nohp);
        img_qr = v.findViewById(R.id.qrcode);
        return v;
    }


}

