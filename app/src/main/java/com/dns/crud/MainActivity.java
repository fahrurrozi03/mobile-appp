package com.dns.crud;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.DialogOnDeniedPermissionListener;
import com.karumi.dexter.listener.single.PermissionListener;
import com.karumi.dexter.listener.single.SnackbarOnDeniedPermissionListener;
import com.williammora.snackbar.Snackbar;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText nama_ku,no_hp_ku,alamat_ku;
    Button tambah_data, dataku;
    TelephonyManager telephonyManager;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (dataUser.getLoggedInStatus(getBaseContext())){
           startActivity(new Intent(getBaseContext(), dashboard.class));
           finish();
        }

        editText = findViewById(R.id.no_hp);

//        nama_ku = findViewById(R.id.nama);
        no_hp_ku = findViewById(R.id.no_hp);
//        alamat_ku = findViewById(R.id.alamat);

        tambah_data = findViewById(R.id.tambah);
        cekPermission();

//        dataku = findViewById(R.id.cekdata);
//        dataku.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(MainActivity.this,tampilsemua.class);
//                startActivity(i);
//            }
//        });
        tambah_data.setOnClickListener(this);
    }
    //Dibawah ini merupakan perintah untuk Menambahkan Pegawai (CREATE)

    private void addEmployee(){


//        final String nama = nama_ku.getText().toString().trim();
        final String no_hp = no_hp_ku.getText().toString().trim();
        final String nopee = editText.getText().toString().trim();

        cekPermission();

        if(nopee.isEmpty()){
            Snackbar.with(MainActivity.this)
                    .text("Nomor Handphone wajib diisi")
                    .actionLabel("OK")
                    .show(MainActivity.this);
        } else {

            cekPermission();
            Intent nope = new Intent(this, verify_otp.class);
            nope.putExtra(konfigurasi.NO_HP, no_hp);
            startActivity(nope);
            finish();
        }

//        final String alamat = alamat_ku.getText().toString().trim();

//        class AddEmployee extends AsyncTask<Void,Void,String> {
//
//            ProgressDialog loading;
//
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//                loading = ProgressDialog.show(MainActivity.this,"Menambahkan...","Tunggu...",false,false);
//            }
//
//            @Override
//            protected void onPostExecute(String s) {
//                super.onPostExecute(s);
//                loading.dismiss();
//                Toast.makeText(MainActivity.this,s,Toast.LENGTH_LONG).show();
//            }
//
//            @Override
//            protected String doInBackground(Void... v) {
//                HashMap<String,String> params = new HashMap<>();
////                params.put(konfigurasi.KEY_EMP_NAMA,nama);
//                params.put(konfigurasi.KEY_EMP_NOHP,no_hp);
////                params.put(konfigurasi.KEY_EMP_ALAMAT,alamat);
//
//                requestHandler rh = new requestHandler();
//                String res = rh.sendPostRequest(konfigurasi.URL_ADD, params);
//                return res;
//            }
//        }
//
//        AddEmployee ae = new AddEmployee();
//        ae.execute();
    }

    private void cekPermission() {

            Dexter.withActivity(this)
                    .withPermission(Manifest.permission.READ_PHONE_STATE)
                    .withListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted(PermissionGrantedResponse response) {
                            telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                            @SuppressLint("MissingPermission") String imei = telephonyManager.getDeviceId();
                            dataUser.setRegisteredImei(getBaseContext(), imei);
                        }
                        @Override
                        public void onPermissionDenied(PermissionDeniedResponse response) {
                            Snackbar.with(MainActivity.this)
                                    .text("Untuk melanjutkan berikan izin akses")
                                    .show(MainActivity.this);

                        }
                        @Override
                        public void onPermissionRationaleShouldBeShown(PermissionRequest permission,
                                                                       PermissionToken token) {
                            Snackbar.with(MainActivity.this)
                                    .text("Untuk melanjutkan berikan izin akses")
                                    .show(MainActivity.this);

                        }
                    })
                    .check();

    }
    @Override
    protected void onResume() {
        super.onResume();
        cekPermission();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        if(v == tambah_data){
            addEmployee();
        }

    }
}
