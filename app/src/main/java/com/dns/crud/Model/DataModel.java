package com.dns.crud.Model;

public class DataModel {

    String id, nama, kode_qr;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKode_qr() {
        return kode_qr;
    }

    public void setKode_qr(String kode_qr) {
        this.kode_qr = kode_qr;
    }
}
