package com.dns.crud.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetMember {

    @SerializedName("id")
    Integer memberId;
    @SerializedName("status")
    String status;
    @SerializedName("result")
    List<Member> listDataMember;
    @SerializedName("message")
    String message;
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public List<Member> getListDataMember() {
        return listDataMember;
    }
    public void setListDataKontak(List<Member> listDataMember) {
        this.listDataMember = listDataMember;
    }
}
