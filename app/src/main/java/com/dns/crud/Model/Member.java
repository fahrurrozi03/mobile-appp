package com.dns.crud.Model;

import com.google.gson.annotations.SerializedName;

public class Member {

    @SerializedName("id")
    private String id;
    @SerializedName("nama")
    private String nama;
    @SerializedName("no_hp")
    private String no_hp;

    public Member(){}

    public Member(String id, String nama, String nomor) {
        this.id = id;
        this.nama = nama;
        this.no_hp = nomor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public void setNo_hp(String no_hp) {
        this.no_hp = no_hp;
    }
}
