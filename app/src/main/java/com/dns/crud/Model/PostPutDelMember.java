package com.dns.crud.Model;

import com.google.gson.annotations.SerializedName;

public class PostPutDelMember {

    @SerializedName("status")
    String status;
    @SerializedName("result")
    Member mMember;
    @SerializedName("message")
    String message;
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public Member getKontak() {
        return mMember;
    }
    public void setKontak(Member Member) {
        mMember = Member;
    }
}
