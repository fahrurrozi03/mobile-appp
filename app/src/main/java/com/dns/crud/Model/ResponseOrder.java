package com.dns.crud.Model;

import java.util.List;

public class ResponseOrder {

    String order_id, order_name, order_type;
    List<DataOrder> result;

    public String getOrder_id() {
        return order_id;
    }

    public String getOrder_name() {
        return order_name;
    }

    public void setOrder_name(String order_name) {
        this.order_name = order_name;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public List<DataOrder> getResult() {
        return result;
    }

    public void setResult(List<DataOrder> result) {
        this.result = result;
    }
}
