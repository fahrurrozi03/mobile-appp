package com.dns.crud;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.dns.crud.Adapter.AdapterData;
import com.dns.crud.Adapter.AdapterOrder;
import com.dns.crud.Model.DataOrder;
import com.dns.crud.Model.ResponseOrder;
import com.dns.crud.Rest.ApiServiceResto;
import com.dns.crud.Rest.RetroServer;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderActivity extends AppCompatActivity {

    private RecyclerView mRecyclerOrder;
    private RecyclerView.Adapter mAdapterOrder;
    private RecyclerView.LayoutManager mManagerOrder;
    private List<DataOrder> mItemsOrder = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        mRecyclerOrder = (RecyclerView) findViewById(R.id.listOrder);
        mManagerOrder  = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerOrder.setLayoutManager(mManagerOrder);

        ApiServiceResto apiServiceResto = RetroServer.getClient().create(ApiServiceResto.class);
        Call<ResponseOrder> getdataOrder = apiServiceResto.getOrder();
        getdataOrder.enqueue(new Callback<ResponseOrder>() {
            @Override
            public void onResponse(Call<ResponseOrder> call, Response<ResponseOrder> response) {
                Log.d("ORDER", "RESPONSE : " + response.body().getOrder_name());
                mItemsOrder = response.body().getResult();
                mAdapterOrder = new AdapterOrder(OrderActivity.this, mItemsOrder);
                mRecyclerOrder.setAdapter(mAdapterOrder);
                mAdapterOrder.notifyDataSetChanged();
                DataOrder dataOrder = new DataOrder();
            }

            @Override
            public void onFailure(Call<ResponseOrder> call, Throwable t) {

                Log.d("RETRO", "FAILED : respon gagal");
            }
        });

    }
}
