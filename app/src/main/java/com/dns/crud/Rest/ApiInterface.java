package com.dns.crud.Rest;

import com.dns.crud.Model.GetMember;
import com.dns.crud.Model.PostPutDelMember;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface ApiInterface {
    @GET("member_android")
    Call<GetMember> getMember();
    @FormUrlEncoded
    @POST("member")
    Call<PostPutDelMember> postMember(@Field("nama") String nama,
                                      @Field("no_hp") String no_hp);
    @FormUrlEncoded
    @PUT("member")
    Call<PostPutDelMember> putMember(@Field("id") String id,
                                     @Field("nama") String nama,
                                     @Field("no_hp") String no_hp);
    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "member", hasBody = true)
    Call<PostPutDelMember> deleteMember(@Field("id") String id);

}
