package com.dns.crud.Rest;

import com.dns.crud.Model.ResponseModel;
import com.dns.crud.Model.ResponseOrder;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface ApiServiceResto {

    @FormUrlEncoded
    @POST("insert.php")
    Call<ResponseModel> sendResto(@Field("nama") String nama,
    @Field("kode_qr") String kode_qr);

    @GET("cekqrcode.php")
    Call<ResponseModel> getResto();

    @GET("cekorder.php")
    Call<ResponseOrder> getOrder();

    @FormUrlEncoded
    @POST("update.php")
    Call<ResponseModel> updateModel(@Field("id") String id,
    @Field("nama") String nama,
    @Field("kode_qr") String kode_qr);

//    @FormUrlEncoded
//    @POST("delete.php")
//    Call<ResponseModel> deleteData(@Field("id") String id);
}
