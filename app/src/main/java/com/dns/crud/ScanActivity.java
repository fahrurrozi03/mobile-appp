package com.dns.crud;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.dns.crud.Model.DataModel;
import com.dns.crud.Model.ResponseModel;
import com.dns.crud.Rest.ApiServiceResto;
import com.dns.crud.Rest.RetroServer;
import com.google.zxing.Result;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScanActivity extends AppCompatActivity{

    private CodeScanner mCodeScanner;
    private CodeScannerView scannerView;
    private ImageView ivBgContent, mFlashon,mBack;
    private CameraManager mCameraManager;
    private TextView mTitle;
    private String cameraId;
    private Boolean isTorchOn;
    private Button toggleButton;
    private boolean flashLightStatus = false;
    private Camera camera;
    private Camera.Parameters parameters;
    private static final int CAMERA_REQUEST = 50;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);


        ivBgContent = findViewById(R.id.ivBgContent);
        scannerView = findViewById(R.id.scannerView);
        mFlashon = findViewById(R.id.onFlash);

        mFlashon.bringToFront();
        ivBgContent.bringToFront();

        final boolean hasCameraFlash = getPackageManager().
                hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
        boolean isEnabled = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED;


        if(isEnabled) {
//            mFlashon.setEnabled(!isEnabled);
            mFlashon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(ScanActivity.this, "ON FLASH", Toast.LENGTH_SHORT).show();
                    ActivityCompat.requestPermissions(ScanActivity.this, new String[] {Manifest.permission.CAMERA}, CAMERA_REQUEST);
                    mCodeScanner.startPreview();
                    flashLightOn();
                }
            });

        } else {
            Toast.makeText(this, "no", Toast.LENGTH_SHORT).show();
        }



        mCodeScanner = new CodeScanner(this, scannerView);
        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String message = result.getText();
                        cekCodeQr(message);
                    }
                });
            }
        });
        checkCameraPermission();
    }

    private void cekCodeQr(String message) {

        final ProgressDialog pd;
        pd = new ProgressDialog(this);
        pd.setCancelable(false);
        pd.show();

        String nama,kode_qr;
        DataModel dm = new DataModel();

        ApiServiceResto api = RetroServer.getClient().create(ApiServiceResto.class);
        Call<ResponseModel> getdata = api.getResto();

        getdata.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                pd.hide();
                Toast.makeText(ScanActivity.this, "berhasil", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), menuActivity.class));

            }
            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d("RETRO", "FAILED : respon gagal");

            }

        });
//        Intent goInput = new Intent(ScanActivity.this, menuActivity.class);
//        goInput.putExtra("id", dm.getId());
//
//        ScanActivity.this.startActivity(goInput);

    }

    private void flashLightOn() {
        mCameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        mCodeScanner.startPreview();

        try {
            String cameraId = mCameraManager.getCameraIdList()[0];
            mCameraManager.setTorchMode(cameraId, true);
            flashLightStatus = true;
            mCodeScanner.startPreview();
        } catch (CameraAccessException e) {

        }
    }

    private void toggleButton() {

        if (isTorchOn) {
            toggleButton.setBackgroundResource(R.drawable.fotomenu);
        }
        else {
            toggleButton.setBackgroundResource(R.drawable.fotomuka);
        }
    }

    private void turnonflash() {

//        if(!isTorchOn) {
//            if(camera == null || parameters == null) {
//                return;
//            }

            parameters = camera.getParameters();
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
            camera.setParameters(parameters);
            camera.startPreview();
            isTorchOn = true;
    }

    private void turnflashoff() {
        if(isTorchOn) {
            if(camera == null || parameters == null) {
                return;
            }

            parameters = camera.getParameters();
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            camera.setParameters(parameters);
            camera.stopPreview();
            isTorchOn = false;
            toggleButton();
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
//        checkCameraPermission();
    }

    @Override
    protected void onPause() {
        mCodeScanner.releaseResources();
        super.onPause();
    }

    private void checkCameraPermission(){
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.CAMERA)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        mCodeScanner.startPreview();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        AlertDialog.Builder alertPermission = new AlertDialog.Builder(ScanActivity.this);

                        alertPermission.setTitle("Peringatan");

                        alertPermission
                                .setMessage("Untuk scan kode QR berikan akses Kamera")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        checkCameraPermission();
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alertDialog = alertPermission.create();

                        alertDialog.show();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission,
                                                                   PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .check();
    }

}
