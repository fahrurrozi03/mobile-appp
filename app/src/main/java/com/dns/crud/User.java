package com.dns.crud;

import com.google.gson.annotations.SerializedName;

/**
 * Created by haerul on 17/03/18.
 */

public class User {

    int id;
    String nama;
    String no_hp;

    public User() {
    }

    public User(int id, String nama, String no_hp) {
        this.id = id;
        this.nama = nama;
        this.no_hp= no_hp;
    }
}