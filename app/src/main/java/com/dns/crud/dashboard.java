package com.dns.crud;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dns.crud.Adapter.PhotoAdapter;
import com.dns.crud.Fragments.AkunFragment;
import com.dns.crud.Fragments.LokasiFragment;
import com.dns.crud.Fragments.OrderFragment;
import com.dns.crud.Fragments.ScanFragment;
import com.dns.crud.Model.DataOrder;
import com.dns.crud.Model.PhotoData;
import com.dns.crud.Rest.ApiClient;
import com.dns.crud.Rest.GetService;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.zxing.WriterException;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.williammora.snackbar.Snackbar;

import java.util.List;

import me.ydcool.lib.qrmodule.encoding.QrGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class dashboard extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private TextView nama, nope;
    private String nohp;
    public TelephonyManager telephonyManager;
    private Button logoutt;
    ProgressBar progressBar;
    private ImageView img_qr, bottomln;
    PorterDuff porterDuff;
    private Button cekDat;
    private FloatingActionButton fabmenu;
    private String JSON_STRING;
    private PhotoAdapter adapter;
    private RecyclerView recyclerView;
    private CardView sliderUp, cardAtas;
    ProgressDialog progressDoalog;
    private BottomAppBar bottom;
    public NavigationView navigationView;

    private String id;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        Intent intent = getIntent();

        id = intent.getStringExtra(konfigurasi.EMP_ID);

        BottomNavigationView bottmNav = findViewById(R.id.bottom_navigation);
        bottmNav.setOnNavigationItemSelectedListener(navListener);

        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);     //  Fixed Portrait orientation


        bottomln = findViewById(R.id.bottomline);
        cardAtas = findViewById(R.id.card);
        nama = findViewById(R.id.name);
        nope = findViewById(R.id.nohp);
        bottom = findViewById(R.id.bottom_app_bar);
        img_qr = findViewById(R.id.qrcode);
        sliderUp = findViewById(R.id.slideratas);

        sliderUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.with(dashboard.this)
                        .text("slider atas")
                        .show(dashboard.this);
            }
        });
        logoutt = findViewById(R.id.logout);
//        cekDat =findViewById(R.id.cek);
        logoutt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataUser.clearLoggedInUser(getBaseContext());
                startActivity(new Intent(getBaseContext(), MainActivity.class));
                finish();
            }
        });
        showUser();
        
        progressDoalog = new ProgressDialog(dashboard.this);
        progressDoalog.setMessage("Loading....");
        progressDoalog.show();

        /*Create handle for the RetrofitInstance interface*/
        GetService service = ApiClient.getRetrofitInstance().create(GetService.class);
        Call<List<PhotoData>> call = service.getAllPhotos();
        call.enqueue(new Callback<List<PhotoData>>() {
            @Override
            public void onResponse(Call<List<PhotoData>> call, Response<List<PhotoData>> response) {
                progressDoalog.dismiss();
                generateDataList(response.body());
            }

            @Override
            public void onFailure(Call<List<PhotoData>> call, Throwable t) {
                progressDoalog.dismiss();
                Toast.makeText(dashboard.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void refreshData() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_scan:
                Intent i = new Intent(this,MainActivity.class);
                this.startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void scanMenu() {
        Toast.makeText(this, "HALO", Toast.LENGTH_SHORT).show();
    }

    private void showUser() {

        nama.setText(dataUser.getRegisteredUser(getBaseContext()));
        nope.setText(dataUser.getLoggedInUser(getBaseContext()));

        String qrcodeimg = dataUser.getRegisteredQrcode(getBaseContext());

        if(qrcodeimg.isEmpty()){

            String qrcode_updt_not = dataUser.getLoggedInUser(getBaseContext());

            Bitmap qrCodes = null;
            try {
                qrCodes = new QrGenerator.Builder()
                        .content(qrcode_updt_not)
                        .qrSize(150)
                        .margin(2)
                        .color(Color.BLACK)
                        .bgColor(Color.WHITE)
                        .ecc(ErrorCorrectionLevel.H)
                        .overlaySize(100)
                        .overlayAlpha(255)
                        .overlayXfermode(PorterDuff.Mode.SRC_ATOP)
                        .encode();
            } catch (WriterException e) {
                e.printStackTrace();
            }
            img_qr.setImageBitmap(qrCodes);


        } else {

            String qrcode_updt = dataUser.getRegisteredQrcode(getBaseContext());

            Bitmap qrCode = null;
            try {
                qrCode = new QrGenerator.Builder()
                        .content(qrcode_updt)
                        .qrSize(150)
                        .margin(2)
                        .color(Color.BLACK)
                        .bgColor(Color.WHITE)
                        .ecc(ErrorCorrectionLevel.H)
                        .overlaySize(100)
                        .overlayAlpha(255)
                        .overlayXfermode(PorterDuff.Mode.SRC_ATOP)
                        .encode();
            } catch (WriterException e) {
                e.printStackTrace();
            }
            img_qr.setImageBitmap(qrCode);
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage("Apakah Kamu Yakin Ingin Keluar?");

            alertDialogBuilder.setPositiveButton("Ya",
                    new DialogInterface.OnClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            finish();
                        }
                    });

            alertDialogBuilder.setNegativeButton("Tidak",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {

                        }
                    });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        return super.onKeyDown(keyCode, event);
    }
    /*Method to generate List of data using RecyclerView with custom adapter*/
    private void generateDataList(List<PhotoData> photoList) {
        recyclerView = findViewById(R.id.customRecyclerView);
        adapter = new PhotoAdapter(this,photoList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(dashboard.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {

            GestureDetector gestureDetector = new GestureDetector(getApplicationContext(), new GestureDetector.SimpleOnGestureListener() {
                public boolean onSingleTapUp(MotionEvent e){
                    return true;
                }
            });

                @Override
            public boolean onInterceptTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {
                    View Child = rv.findChildViewUnder(e.getX(), e.getY());
                    if(Child != null && gestureDetector.onTouchEvent(e)){
                        int position = rv.getChildAdapterPosition(Child);

                        Toast.makeText(dashboard.this, "Title : ", Toast.LENGTH_SHORT).show();
                    }

                    return false;
            }

            @Override
            public void onTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selectedFragment = null;
                    Intent scan = null;

                    switch (menuItem.getItemId()) {
                        case R.id.action_scan:
                            Intent First = new Intent(dashboard.this, ScanActivity.class);
                            startActivity(First);
                            break;
                        case R.id.action_acc:
                            Intent akun = new Intent(dashboard.this, Akun.class);
                            startActivity(akun);
                            break;
                        case R.id.action_locy:
                            selectedFragment = new LokasiFragment();
                            cardAtas.setVisibility(View.GONE);
                            break;
                        case R.id.action_favorites:
                            Intent order = new Intent(dashboard.this, OrderActivity.class);
                            startActivity(order);
                            break;
//                            selectedFragment = new OrderFragment();
//                            cardAtas.setVisibility(View.GONE);
//                            break;
                        case R.id.action_recents:
                            startActivity(getIntent());
                            return true;
                    }
//                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
//                            selectedFragment).commit();
                    return true;
                }
            };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {

            case 12:
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivity(new Intent(getApplicationContext(), ScanActivity.class));
                    finish();
                } else {

                    checkPermission();
                }

                break;
        }
    }

    public boolean checkPermission() {

        int result = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA);
        return result == PackageManager.PERMISSION_GRANTED;

    }

    private void scanPermission() {

        ActivityCompat.requestPermissions(dashboard.this, new String[]{Manifest.permission.CAMERA},12);

        Intent First = new Intent(dashboard.this, ScanActivity.class);
        startActivity(First);
    }

    private void newscan() {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_scan:

        }
        return true;
    }

}
