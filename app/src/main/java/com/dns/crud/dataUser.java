package com.dns.crud;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class dataUser {
    static final String KEY_USER_TEREGISTER = "user", KEY_OTP = "otp";
    static final String KEY_USER_PHONE  = "phone_logged_in";
    static final String KEY_USER_ID  = "id";
    static final String KEY_USER_IMEI  = "imei_logged_in";
    static final String KEY_USER_EMAIL  = "email_logged_in";
    static final String KEY_STATUS_LOGIN    = "status_logged_in";
    static final String KEY_USER_QRCODE  = "status_qrcode";

    // pendkelarasian shared prefrences yang berdasarkan parameter context
    private static SharedPreferences getSharedPreferences(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }
    /** Deklarasi Edit Preferences dan mengubah data
     *  yang memiliki key isi KEY_USER_TEREGISTER dengan parameter username */
    public static void setRegisteredUser(Context context, String username){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(KEY_USER_TEREGISTER, username);
        editor.apply();
    }
    /** Mengembalikan nilai dari key KEY_USER_TEREGISTER berupa String */
    public static String getRegisteredUser(Context context){
        return getSharedPreferences(context).getString(KEY_USER_TEREGISTER,"");
    }

    /** Deklarasi Edit Preferences dan mengubah data
     *  yang memiliki key KEY_OTP dengan parameter otp */
    public static void setRegisteredOtp(Context context, String kode_otp){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(KEY_OTP, kode_otp);
        editor.apply();
    }
    /** Mengembalikan nilai dari key KEY_PASS_TEREGISTER berupa String */
    public static String getRegisteredOtp(Context context){
        return getSharedPreferences(context).getString(KEY_OTP,"");
    }
    public static void setRegisteredImei(Context context, String imei){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(KEY_USER_IMEI, imei);
        editor.apply();
    }
    public static String getRegisteredImei(Context context){
        return getSharedPreferences(context).getString(KEY_USER_IMEI,"");
    }
    public static void setRegisteredEmail(Context context, String email){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(KEY_USER_EMAIL, email);
        editor.apply();
    }
    public static String getRegisteredEmail(Context context){
        return getSharedPreferences(context).getString(KEY_USER_EMAIL,"");
    }
    public static void setId(Context context, String id){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(KEY_USER_ID, id);
        editor.apply();
    }
    public static String getId(Context context){
        return getSharedPreferences(context).getString(KEY_USER_ID,"");
    }
    /** Deklarasi Edit Preferences dan mengubah data
     *  yang memiliki key KEY_USERNAME_SEDANG_LOGIN dengan parameter username */
    public static void setLoggedInUser(Context context, String phone){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(KEY_USER_PHONE, phone);
        editor.apply();
    }
    /** Mengembalikan nilai dari key KEY_USERNAME_SEDANG_LOGIN berupa String */
    public static String getLoggedInUser(Context context){
        return getSharedPreferences(context).getString(KEY_USER_PHONE,"");
    }


    public static void setRegisteredQrcode(Context context, String qrcodeString){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(KEY_USER_QRCODE, qrcodeString);
        editor.apply();
    }
    /** Mengembalikan nilai dari key KEY_USER_TEREGISTER berupa String */
    public static String getRegisteredQrcode(Context context){
        return getSharedPreferences(context).getString(KEY_USER_QRCODE,"");
    }

    /** Deklarasi Edit Preferences dan mengubah data
     *  yang memiliki key KEY_STATUS_SEDANG_LOGIN dengan parameter status */
    public static void setLoggedInStatus(Context context, boolean status){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(KEY_STATUS_LOGIN,status);
        editor.apply();
    }
    /** Mengembalikan nilai dari key KEY_STATUS_SEDANG_LOGIN berupa boolean */
    public static boolean getLoggedInStatus(Context context){
        return getSharedPreferences(context).getBoolean(KEY_STATUS_LOGIN,false);
    }

    /** Deklarasi Edit Preferences dan menghapus data, sehingga menjadikannya bernilai default
     *  khusus data yang memiliki key KEY_USERNAME_SEDANG_LOGIN dan KEY_STATUS_SEDANG_LOGIN */
    public static void clearLoggedInUser (Context context){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.remove(KEY_USER_PHONE);
        editor.remove(KEY_USER_TEREGISTER);
        editor.remove(KEY_STATUS_LOGIN);
        editor.apply();
    }
}
