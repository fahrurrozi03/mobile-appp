package com.dns.crud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SyncContext;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.williammora.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.prefs.Preferences;

public class datadiri extends AppCompatActivity implements View.OnClickListener {

    private EditText user;
    private String nohp,otp;
    private Button ljt;
    private String id;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private String JSON_STRING;
    private static final String URL_GET_USER = "http://192.168.100.34/mobile/showuser.php?id=";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datadiri);

        Intent intent = getIntent();
        nohp = intent.getStringExtra(konfigurasi.NO_HP);
        otp = intent.getStringExtra(konfigurasi.OTP);

        sharedPreferences = getSharedPreferences("dataMember", Context.MODE_PRIVATE);
        user = findViewById(R.id.username);
        ljt  = findViewById(R.id.lanjut2);

        ljt.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == ljt) {
            addMember();
        }
    }

    private void addMember() {
        //mendapatkan input dari user
        String getUser = user.getText().toString();
        String getNope = nohp;
        String getOtp  = otp;

        if(getUser.isEmpty()){
            Snackbar.with(datadiri.this)
                    .text("Masukkan Username kamu")
                    .show(datadiri.this);
        } else {
            dataUser.setLoggedInStatus(getBaseContext(), true);
            dataUser.setRegisteredUser(getBaseContext(), getUser);
            dataUser.setRegisteredOtp(getBaseContext(), getOtp);
            dataUser.setLoggedInUser(getBaseContext(), getNope);
            addAllData();
            startActivity(new Intent(datadiri.this, dashboard.class));
            finish();
        }
    }

    private void addAllData() {

            Intent intent = getIntent();

            id = intent.getStringExtra(konfigurasi.EMP_ID);

            class AddEmployee extends AsyncTask<Void,Void,String> {

                ProgressDialog loading;

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    loading = ProgressDialog.show(datadiri.this,"Loading...","sedang menyiapkan",false,false);
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    loading.dismiss();
                    Toast.makeText(datadiri.this,"sudah siap",Toast.LENGTH_SHORT).show();
                }

                @Override
                protected String doInBackground(Void... v) {
                    HashMap<String,String> params = new HashMap<>();
                    params.put(konfigurasi.KEY_EMP_NAMA,dataUser.getRegisteredUser(getBaseContext()));
                    params.put(konfigurasi.KEY_EMP_ID,dataUser.getRegisteredUser(getBaseContext()));
                    params.put(konfigurasi.KEY_EMP_NOHP,dataUser.getLoggedInUser(getBaseContext()));
                    params.put(konfigurasi.KEY_EMP_KODEOTP,dataUser.getRegisteredOtp(getBaseContext()));
                    String random = dataUser.getRegisteredImei(getBaseContext()) + dataUser.getLoggedInUser(getBaseContext());
                    params.put(konfigurasi.KEY_EMP_KODE_QR,random);
                    params.put(konfigurasi.KEY_EMP_IMEI,dataUser.getRegisteredImei(getBaseContext()));

                    requestHandler rh = new requestHandler();
                    String res = rh.sendPostRequest(konfigurasi.URL_ADD, params);
                    return res;
                }
            }

            AddEmployee ae = new AddEmployee();
            ae.execute();

    }

//    private void pushdata() {
//        JSONObject jsonObject = null;
//        ArrayList<HashMap<String,String>> list = new ArrayList<HashMap<String, String>>();
//        try {
//            jsonObject = new JSONObject(JSON_STRING);
//            JSONArray result = jsonObject.getJSONArray(konfigurasi.TAG_JSON_ARRAY);
//
//            for(int i = 0; i<result.length(); i++){
//                JSONObject jo = result.getJSONObject(i);
//                String id = jo.getString(konfigurasi.TAG_ID);
//                String no_hp = jo.getString(konfigurasi.TAG_NAMA);
//
//                HashMap<String,String> employees = new HashMap<>();
//                employees.put(konfigurasi.TAG_ID,id);
//                employees.put(konfigurasi.TAG_NAMA,no_hp);
//                list.add(employees);
//            }
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }
}
