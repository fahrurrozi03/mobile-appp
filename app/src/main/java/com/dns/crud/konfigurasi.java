package com.dns.crud;

public class konfigurasi {

    public static final String URL_ADD = "http://192.168.100.34/mobile/daftar.php";
    public static final String URL_GET_ALL = "http://192.168.100.34/mobile/readall.php";
    public static final String URL_GET_EMP = "http://192.168.100.34/mobile/read.php?id=";
    public static final String URL_UPDATE_EMP = "http://192.168.100.34/mobile/update.php";
    public static final String URL_DELETE_EMP = "http://192.168.100.34/mobile/delete.php?id=";

    //Dibawah ini merupakan Kunci yang akan digunakan untuk mengirim permintaan ke Skrip PHP
    public static final String KEY_EMP_ID = "id";
    public static final String KEY_EMP_NAMA = "nama";
    public static final String KEY_EMP_IMEI= "imei";
    public static final String KEY_EMP_KODEOTP = "kode_otp";
    public static final String KEY_EMP_NOHP = "no_hp";
    public static final String KEY_EMP_KODE_QR = "kode_qr";
    public static final String KEY_EMP_ALAMAT = "alamat";

    //JSON Tags
    public static final String TAG_JSON_ARRAY="result";
    public static final String TAG_ID = "id";
    public static final String TAG_NAMA = "nama";
    public static final String TAG_NOHP = "no_hp";
    public static final String TAG_ALAMAT = "alamat";

    //ID
    public static final String EMP_ID = "emp_id";
    public static final String NO_HP = "nohp";
    public static final String OTP = "otp";

}
