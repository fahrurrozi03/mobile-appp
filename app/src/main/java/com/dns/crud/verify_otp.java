package com.dns.crud;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.williammora.snackbar.Snackbar;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

public class verify_otp extends AppCompatActivity implements View.OnClickListener {

    private String nohp;
    private Button lanjut1;
    private EditText otpkode;
    private TextView nop;
    int randomNumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);

        Intent intent = getIntent();
        nohp = intent.getStringExtra(konfigurasi.NO_HP);

        otpkode = findViewById(R.id.otp);
        nop = findViewById(R.id.ashp);
        nop.setText(nohp);

        lanjut1 = findViewById(R.id.lanjut1);
        lanjut1.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == lanjut1){
            addMember();
        }

    }

//    public String sendSms() {
//        try {
//            // Construct data
//            String apiKey = "apikey=" + "ORGLnm7daAk-QiDFKF4Iba1lp7p7CFRkpS1fZnWRHH";
//            Random random = new Random();
//            randomNumber = random.nextInt(999999);
//            String message = "&message=" + "Hey " + nohp + " ini adalah KODE OTP anda " + randomNumber;
//            String sender = "&sender=" + "DNS";
//            String numbers = "&numbers=" + nohp;
//
//            // Send data
//            HttpURLConnection conn = (HttpURLConnection) new URL("https://api.txtlocal.com/send/?").openConnection();
//            String data = apiKey + numbers + message + sender;
//            conn.setDoOutput(true);
//            conn.setRequestMethod("POST");
//            conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
//            conn.getOutputStream().write(data.getBytes("UTF-8"));
//            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//            final StringBuffer stringBuffer = new StringBuffer();
//            String line;
//            while ((line = rd.readLine()) != null) {
//                stringBuffer.append(line);
//            }
//            rd.close();
//            Toast.makeText(this, "OTP berhasil dikirim", Toast.LENGTH_SHORT).show();
//
////            return stringBuffer.toString();
//        } catch (Exception e) {
////            System.out.println("Error SMS "+e);
////            return "Error "+e;
//            Toast.makeText(this, "Error SMS" + e, Toast.LENGTH_SHORT).show();
//        }
//    }
//
//    private void verifyOtp() {
//        if(randomNumber == Integer.valueOf(otpkode.getText().toString().trim())){
//            Toast.makeText(this, "Sucessfully", Toast.LENGTH_SHORT).show();
//            addMember();
//        } else {
//            Toast.makeText(this, "Otp Salah, silahkan coba lagi", Toast.LENGTH_SHORT).show();
//        }
//    }



    private void addMember() {

        final String kode_otp = otpkode.getText().toString().trim();
        final String no_hp = nohp;

        if(kode_otp.isEmpty()){
            Snackbar.with(verify_otp.this)
                    .text("Masukkan OTP yang sudah dikirim")
                    .show(verify_otp.this);

        } else {

            Intent nope = new Intent(this, datadiri.class);
            nope.putExtra(konfigurasi.NO_HP, no_hp);
            nope.putExtra(konfigurasi.OTP, kode_otp);
            startActivity(nope);
            finish();

        }
    }

}

